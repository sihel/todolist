<?php use yii\helpers\Html; ?>
<div class="pull-right btn-group">
	<?php echo Html::a('All', array('entry/index'), array('class' => 'btn btn-info')); ?>
    <?php echo Html::a('Update', array('entry/update', 'id' => $entry->id), array('class' => 'btn btn-primary')); ?>
    <?php echo Html::a('Delete', array('entry/delete', 'id' => $entry->id), array('class' => 'btn btn-danger', 'onclick' => "return confirm('Вы точно хотите удалить сообщение?')")); ?>
</div>
 
<h1><?php echo $entry->title; ?></h1>
<p><?php echo $entry->text; ?></p>
<hr />
<p>Status: <?php echo $entry->getStatus(); ?></p>