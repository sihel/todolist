<?php use yii\helpers\Html; ?>
 
<?php echo Html::a('Create New Entry', array('entry/create'), array('class' => 'btn btn-primary pull-right')); ?>
<div class="clearfix"></div>
<hr />
<?php if(Yii::$app->session->hasFlash('EntryDeletedError')): ?>
	<div class="alert alert-error">
	    There was an error deleting your TODO entry!
	</div>
	<?php endif; ?>
	 
	<?php if(Yii::$app->session->hasFlash('EntryDeleted')): ?>
	<div class="alert alert-success">
	    Your TODO entry has successfully been deleted!
	</div>
<?php endif; ?>
<table class="table table-striped table-hover">
    <tr>
        <td>#</td>
        <td>Title</td>
        <td>Text</td>
        <td>Status</td>
        <td>Options</td>
    </tr>
    <?php foreach ($data as $entry): ?>
        <tr>
            <td>
                <?php echo Html::a($entry->id, array('entry/read', 'id'=>$entry->id)); ?>
            </td>
            <td><?php echo Html::a($entry->title, array('entry/read', 'id'=>$entry->id)); ?></td>
            <td><?php echo $entry->text; ?></td>
            <td><?php echo $entry->getStatus(); ?></td>
            <td>
                <?php echo Html::a('Edit', array('entry/update', 'id'=>$entry->id), array('class'=>'icon icon-edit')); ?>
                <?php echo Html::a('Del', array('entry/delete', 'id'=>$entry->id), array('class'=>'icon icon-trash', 'onclick' => "return confirm('Вы точно хотите удалить сообщение?')")); ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>