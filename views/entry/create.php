<?php use yii\helpers\Html;
 	  use app\models\Entry; ?>
 
<?php $form = \yii\widgets\ActiveForm::begin(array(
    'options' => array('class' => 'form-horizontal'),
)); ?>
    <?php echo $form->field($model, 'title')->textInput(array('class' => 'span8 input-sm')); ?>
    <?php echo $form->field($model, 'text')->textArea(array('class' => 'span8 input-sm', 'rows'=>3)); ?>
    <?php echo $form->field($model, 'status')->dropDownList(array(
    	1 => Entry::statusName(1),
    	2 => Entry::statusName(2),
    	0 => Entry::statusName(0),
    ), array('class' => 'form-control input-sm')); ?>
    <div class="form-actions">
        <?php echo Html::submitButton('Create', array('class' => 'btn btn-primary')); ?>
    </div>
<?php \yii\widgets\ActiveForm::end(); ?>