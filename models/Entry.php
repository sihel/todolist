<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entry".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property integer $status
 */
class Entry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'entry';
    }
    
    public static function statusName($st)
    {
	    switch ($st)
    	{
    		case 1:
    			return 'In work';
    			break;
    		case 2:
    			return 'Done';
    			break;
    		case 0:
    			return 'Reject';
    			break;
    		default:
    			return "";
    	}
    }
    
    public function getStatus()
    {
    	return Entry::statusName($this->status);
	}
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[['status', 'title', 'text'], 'required'],
            [['status'], 'integer'],
            [['title'], 'string', 'max' => 45],
            [['text'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'text' => 'Text',
            'status' => 'Status',
        ];
    }
}
