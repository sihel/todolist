<?php

namespace app\controllers;
use Yii;
use app\models\Entry;
use \yii\base\HttpException;

class EntryController extends \yii\web\Controller
{
    public function actionIndex()
	{
	    $post = new Entry;
	    $data = $post->find()->all();
	    echo $this->render('index', array(
	        'data' => $data
	    ));
	}
	
	public function actionRead($id=NULL)
	{
	    if ($id === NULL)
            throw new HttpException(404, 'Not Found');
 
        $entry = Entry::find($id);
 
        if ($entry === NULL)
            throw new HttpException(404, 'Document Does Not Exist');
 
        echo $this->render('read', array(
            'entry' => $entry
        ));
	}
	
	public function actionDelete($id=NULL)
	{
		if ($id === NULL)
	    {
	        Yii::$app->session->setFlash('EntryDeletedError');
	        Yii::$app->getResponse()->redirect(array('entry/index'));
	    }
	 
	    $entry = Entry::find($id);
	 
	 
	    if ($entry === NULL)
	    {
	        Yii::$app->session->setFlash('EntryDeletedError');
	        Yii::$app->getResponse()->redirect(array('entry/index'));
	    }
	 
	    $entry->delete();
	 
	    Yii::$app->session->setFlash('EntryDeleted');
	    Yii::$app->getResponse()->redirect(array('entry/index'));
	}
	
	public function actionCreate()
	{
	    $model = new Entry;
	    if (isset($_POST['Entry']))
	    {
			$model->attributes = $_POST['Entry'];
	 
	        if ($model->save())
	        {
	        	if (!isset($_POST['ajax'])) Yii::$app->getResponse()->redirect(array('entry/read', 'id' => $model->id));
		        return;
		    }
	    }
		
	    echo $this->render('create', array(
	        'model' => $model
	    ));
	}
	
	public function actionUpdate($id=NULL)
	{
	    if ($id === NULL)
	        throw new HttpException(404, 'Not Found');
	 
	    $model = Entry::find($id);
	 
	    if ($model === NULL)
	        throw new HttpException(404, 'Entry Does Not Exist');
	 
	    if (isset($_POST['Entry']))
	    {
	        $model->attributes = $_POST['Entry'];
	 
	        if ($model->save())
	        {
	            Yii::$app->getResponse()->redirect(array('entry/read', 'id' => $model->id));
	            return;
	        }
	    }
	 
	    echo $this->render('update', array(
	        'model' => $model
	    ));
	}
}
